#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import environ
from json import dumps
from os.path import basename
from argparse import ArgumentParser
from requests.api import request

'''
Canvas API token ellenorzese
'''
if 'CANVAS_API_TOKEN' not in environ:
    raise AttributeError('> the CANVAS_API_TOKEN environmental variable is not properly set')
canvasApiToken = environ['CANVAS_API_TOKEN']

'''
Canvas URL beallitasa, opcionalisan kornyezeti valtozobol
'''
canvasUrl = environ['CANVAS_URL'] if 'CANVAS_URL' in environ else 'https://canvas2.cs.ubbcluj.ro'
            

def fetch(method='GET', url=None, endpoint='/', body=None, headers={}, files=None, jsonBody=False):
    '''
    http hivast intez a Canvas szerverhez
    '''
    headers['Authorization'] = 'Bearer %s' %(canvasApiToken)

    if url is None:
        url = '%s/api/v1%s' %(canvasUrl, endpoint)
        
    print('> http request: %s %s %s' %(method, url, body))

    if jsonBody:
        response = request(method, url, json=body, headers=headers)
    else:
        response = request(method, url, data=body, headers=headers, files=files)

    print('> http response: %d %s' %(response.status_code, response.reason))

    if response.headers['Content-Type'].startswith('application/json'):
        responseBody = response.json()
        # print('http valasztest: %s' %(dumps(responseBody, indent=4, sort_keys=True)))
        # print()
    else:
        responseBody = response.content

    if response.status_code >= 200 and response.status_code <= 204:
        return response, responseBody
    else:
        raise AttributeError('> http request error %d %s, response: %s' %(response.status_code, response.reason, dumps(responseBody, indent=4, sort_keys=True)))


def upload(fileName, courseId, assignmentId):
    '''
    allomany feltoles API endpointon keresztul
    '''
    print('> uploading file: %s' %(fileName))
    
    # 1.: allomanyfeltoltes kerese
    
    endpoint = '/courses/%d/assignments/%d/submissions/self/files' %(courseId, assignmentId)
    body = { 'name': basename(fileName) }

    _, responseBody = fetch(method='POST', endpoint=endpoint, body=body)

    print('> receiving meta response')

    # 2. resz: tenyleges allomanyfeltoltes

    url = responseBody['upload_url']
    body = responseBody['upload_params']
    
    files={'file': open(fileName, 'rb')}
    
    print('> uploading file')
    _, responseBody = fetch(method='POST', url=url, body=body, files=files)

    print('> file uploaded successfully, ID = %d' %(responseBody['id']))
    fileId = responseBody['id']

    # 3. resz: submission leadasa az allomannyal

    endpoint = '/courses/%d/assignments/%d/submissions' %(courseId, assignmentId)
    body = {
        'submission': {
            'submission_type': 'online_upload',
            'file_ids': [ fileId ]
        }
    }
    _, responseBody = fetch(method='POST', endpoint=endpoint, body=body, jsonBody=True)

    print('> assignment submitted successfully: %s' %(responseBody['attachments'][0]['filename']))


            
if __name__ == '__main__':
        
    # parse command line arguments
    parser = ArgumentParser(description='Canvas uploader')

    parser.add_argument('fileName', help='filename to be uploaded to Canvas assignment')
    parser.add_argument('courseId', type=int, help='Canvas course ID')
    parser.add_argument('assignmentId', type=int, help='Unique ID of Canvas assignment')

    args = parser.parse_args()
    
    # perform sync
    upload(**vars(args))
    