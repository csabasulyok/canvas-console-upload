#!/bin/bash

# download git repo
git clone https://gitlab.com/csabasulyok/canvas-console-upload.git ~/canvas-console-upload

# install python dependencies
pip install --user -r ~/canvas-console-upload/requirements.txt

# move runnable to bin folder
mkdir -p ~/bin
cp ~/canvas-console-upload/upload.py ~/bin/upload
chmod u+x ~/bin/upload

# write token setting into .bashrc
echo "export CANVAS_API_TOKEN=$CANVAS_API_TOKEN" >>~/.bashrc

# test app
upload -h

echo "Siker! Használati információ fennebb..."