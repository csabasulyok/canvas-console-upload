# canvas-console-upload

## Telepítés

### 1. Canvas elérési token generálása

- Első lépésként generálnunk kell egy egyedi kulcsot, melynek segítségével API-hívásokat intézhetünk a Canvas szerverhez. Ennek érdekében:
    - Navigáljunk a *Beállítások*hoz (*Settings*)  
      ![](img/1.png)
    - Az *Approved Integrations* szekcióban generáljunk magunknak egy új *Access Token*t  
      ![](img/2.png)  
      ![](img/3.png)
      ![](img/4.png)
    - A generált tokent **biztonságosan mentsük el**, mivel utólag nem lehet visszanézni. A token megfelel a jelszavunknak, szóval **ne mutassuk meg senkinek**.  

### 2. `CANVAS_API_TOKEN` környezeti változó beállítása

- A lekért tokent állítsuk be `CANVAS_API_TOKEN` környezeti változóként a kívánt szerveren.

```bash
export CANVAS_API_TOKEN=tokenideabcd
```

- Ellenőrizzük a beállítást

```bash
echo $CANVAS_API_TOKEN
```

### 3. Szkript letöltése

- Ugyanabban a bash konzolban, ahol a `CANVAS_API_TOKEN` változót beállítottuk, töltsük le a mellékelt `install.sh` szkriptet s futtassuk.

```bash
curl https://gitlab.com/csabasulyok/canvas-console-upload/-/raw/master/install.sh | bash
```

- A szkript az `upload` név alatt működtethető, teszteljük:

```bash
upload -h
```

## Feltöltés

### 1. Canvas kurzus és laborfeladat azonosítása

- A Canvas minden kurzusnak és laborfeladatnak generál egy egyedi azonosítót. Ezt a feladathoz navigálva kereshetjük ki. Ennek a példának a tantárgy ID-ja `282`, míg a feladat azonosítója `3443`.  
  ![](img/5.png)

### 2. Egy állomány feltöltése

- A feltöltésre kész állományt töltsük fel az `upload.py` paranccsal. A parancs 3 paramétert vár:
    1. A feltöltendő állomány neve
    2. A Canvas tantárgy-azonosító
    3. A Canvas laborfeladat-azonosító

```bash
upload assignment.cpp 282 3443
```